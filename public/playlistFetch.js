var apiUrl = 'https://azura.rlab.be/api'

function apiFetch(path) {
  return fetch(apiUrl + path).then(r => r.json())
}

function getPlaylist(callback) {
  apiFetch('/nowplaying').then(data => {
    // usar data
    console.log(data)
    let station = data[0]
    let playlist = station.now_playing.playlist
    callback(playlist)
  })
}

// chequeo si el loading tarda +5seg y oculto el footer

const spanCheck = document.querySelector('#playlist-name');
const playerWrap = document.querySelector('.player-wrap');
const playlistData = document.querySelector('.playlist-data');

function checkLoading() {
  setTimeout(function () {
    if (spanCheck.innerHTML == '{cargando..}') {
      const iframe = document.querySelector('iframe');
      iframe.style.display = 'none';
      playerWrap.innerHTML = '<div>error al cargar la radio (volveremos pronto)</div>';
      playerWrap.style.color = '#fff';
      playerWrap.style.margin = '1rem 0';
      playerWrap.style.backgroundColor = 'unset';
      playlistData.innerHTML = '';
    }
  }, 5000);
}

checkLoading();