# Front player rlab radio

Hasta tanto podamos hacer una web app más interesante, armamos este gitlab page estático para tener un reproductor de la radio.

![Muestra del front con reproductor de radio y logo de rlab](./public/img/snapshot.jpg)

Más info del proyecto Rlab Radio [en el repo correspondiente](https://gitlab.com/rlyehlab/rlyeh-radio)

## Links

- [Radio.rlab](https://radio.rlab.be/)
- [Servidor Azuracast instalado](https://azura.rlab.be/login)
- [Public Page Azuracast](https://azura.rlab.be/public/rlab_radio)

Te interesa participar en la radio? Hay distintos roles posibles:

- admin radio azuracast (puede subir archivos, modificar playlists y más, desde la GUI de azuraCast)
- uploader (puede subir archivos via sftp con FileZilla o similares)
- Dj (puede transmitir en vivo desde su compu conectándose al server como invitadx)

Contactá un admin para que te generen acceso.

wip .-.



